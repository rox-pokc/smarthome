<?php

namespace App\Helper;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

trait AuthorTrait
{
    /**
     * @var UserInterface
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     */
    protected $author;

    /**
     * @var integer
     * @ORM\Column(type="integer", nullable=false, options={"default": 0})
     */
    protected $createdAt;

    /**
     * @return UserInterface
     */
    public function getAuthor(): ?UserInterface
    {
        return $this->author;
    }

    /**
     * @param UserInterface $author
     */
    public function setAuthor(UserInterface $author): void
    {
        $this->author = $author;
    }

    /**
     * @return int
     */
    public function getCreatedAt(): ?int
    {
        return $this->createdAt;
    }

    /**
     * @param int $createdAt
     */
    public function setCreatedAt(int $createdAt): void
    {
        $this->createdAt = $createdAt;
    }
}