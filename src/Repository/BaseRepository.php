<?php

namespace App\Repository;

use App\Service\TokenStorageService;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

abstract class BaseRepository extends ServiceEntityRepository
{
    protected $author;

    public function __construct(ManagerRegistry $registry, $entityClass, TokenStorageService $service)
    {
        $this->author = $service->getUser();
        parent::__construct($registry, $entityClass);
    }

    public function findAll()
    {
        $qb = $this->createQueryBuilder('e');
        $qb->andWhere($qb->expr()->eq('e.author', ':author'));
        $qb->setParameter(':author', $this->author);
        return $qb
            ->getQuery()
            ->getResult();
    }

    public function counting()
    {
        $qb = $this->createQueryBuilder('e');
        return $qb
            ->select('count(e.id)')
            ->getQuery()
            ->getResult()[0][1];
    }
}