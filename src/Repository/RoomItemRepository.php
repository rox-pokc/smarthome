<?php

namespace App\Repository;

use App\Entity\RoomItem;
use App\Service\TokenStorageService;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method RoomItem|null find($id, $lockMode = null, $lockVersion = null)
 * @method RoomItem|null findOneBy(array $criteria, array $orderBy = null)
 * @method RoomItem[]    findAll()
 * @method RoomItem[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RoomItemRepository extends BaseRepository
{
    public function __construct(ManagerRegistry $registry, TokenStorageService $service)
    {
        parent::__construct($registry, RoomItem::class, $service);
    }

    // /**
    //  * @return RoomItem[] Returns an array of RoomItem objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?RoomItem
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
