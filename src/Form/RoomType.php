<?php


namespace App\Form;


use App\Entity\Home;
use App\Entity\Room;
use App\Repository\HomeRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RoomType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $author = $options['postedBy'];
        $home = $options['home'];
        $builder
            ->add('name', null, [
                'label' => 'Название'
            ])
            ->add('home', EntityType::class, [
                'label' => 'Дом',
                'class' => Home::class,
                'query_builder' => function (HomeRepository $er) use ($author, $home) {
                    $qb = $er->createQueryBuilder('u');
                    $qb->andWhere($qb->expr()->eq('u.author', ':author'));
                    $qb->setParameter(':author', $author);
                    if ($home != 0) {
                        $qb->andWhere($qb->expr()->eq('u.id', ':home'));
                        $qb->setParameter(':home', $home);
                    }
                    return $qb;
                },
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Room::class,
            'postedBy' => null,
            'home' => null
        ]);
    }
}
