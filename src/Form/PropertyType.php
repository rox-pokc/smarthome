<?php


namespace App\Form;


use App\Entity\Property;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PropertyType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', null, [
                'label' => 'Название'
            ])
            ->add('type', ChoiceType::class, [
                'choices' => [
                    'integer' => 'integer',
                    'boolean' => 'boolean'
                ]
            ])
            ->add('min', IntegerType::class, [
                'label' => 'Минимальное значение',
                'required' => false
            ])
            ->add('max', IntegerType::class, [
                'label' => 'Максимальное значение',
                'required' => false
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Property::class
        ]);
    }
}
