<?php


namespace App\Form;


use App\Entity\Item;
use App\Entity\Property;
use App\Repository\PropertyRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ItemType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $author = $options['postedBy'];
        $builder
            ->add('name', null, [
                'label' => 'Название'
            ])
            ->add('properties', EntityType::class, [
                'label' => 'Свойства',
                'class' => Property::class,
                'query_builder' => function (PropertyRepository $er) use ($author) {
                    $qb = $er->createQueryBuilder('u');
                    $qb->andWhere($qb->expr()->isNull('u.author'))->orWhere($qb->expr()->eq('u.author', ':author'));
                    $qb->setParameter(':author', $author);

                    return $qb;
                },
                'expanded' => true,
                'multiple' => true,
                'by_reference' => false
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Item::class,
            'postedBy' => null
        ]);
    }
}
