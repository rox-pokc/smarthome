<?php


namespace App\Controller;


use App\Entity\Home;
use App\Form\HomeType;
use App\Repository\HomeRepository;
use App\Repository\RoomItemRepository;
use App\Repository\RoomRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/home")
 */
class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home_index", methods={"GET"})
     */
    public function index(HomeRepository $homeRepository): Response
    {
        return $this->render('home/index.html.twig', [
            'homes' => $homeRepository->findAll(),
            'user' => $this->getUser()
        ]);
    }

    /**
     * @Route("/new", name="home_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $home = new Home();
        $form = $this->createForm(HomeType::class, $home);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $home->setUser($this->getUser());
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($home);
            $entityManager->flush();

            return $this->redirectToRoute('home_index');
        }

        return $this->render('home/new.html.twig', [
            'home' => $home,
            'form' => $form->createView(),
            'user' => $this->getUser()
        ]);
    }

    /**
     * @Route("/{id}", name="home_show", methods={"GET"})
     */
    public function show(Home $home, RoomItemRepository $roomItemRepository, RoomRepository $roomRepository): Response
    {
        $fullRooms = array();
        $fullRoom = array();
        $rooms = $roomRepository->findBy(array('home' => $home->getId()));
        foreach ($rooms as $room) {
            $items = $roomItemRepository->findBy(array('author' => $this->getUser()->getId(), 'room' => $room->getId()));
            $fullRoom = array('room' => $room, 'items' => $items);
            array_push($fullRooms, $fullRoom);
        }
        return $this->render('home/show.html.twig', [
            'home' => $home,
            'fullRooms' => $fullRooms,
            'user' => $this->getUser()
        ]);
    }

    /**
     * @Route("/{id}/edit", name="home_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Home $home): Response
    {
        $form = $this->createForm(HomeType::class, $home);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('home_index');
        }

        return $this->render('home/edit.html.twig', [
            'home' => $home,
            'form' => $form->createView(),
            'user' => $this->getUser()
        ]);
    }

    /**
     * @Route("/{id}", name="home_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Home $home): Response
    {
        if ($this->isCsrfTokenValid('delete'.$home->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($home);
            $entityManager->flush();
        }

        return $this->redirectToRoute('home_index');
    }
}
