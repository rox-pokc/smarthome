<?php


namespace App\Controller;


use App\Entity\Item;
use App\Entity\Value;
use App\Form\ItemType;
use App\Repository\ItemRepository;
use App\Repository\PropertyRepository;
use App\Repository\RoomItemRepository;
use App\Repository\ValueRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/item")
 */
class ItemController extends AbstractController
{
    /**
     * @Route("/", name="item_index", methods={"GET"})
     */
    public function index(ItemRepository $itemRepository): Response
    {
        $items = $itemRepository->findAll();
        $defaultItems = $itemRepository->findBy(array('author' => null));
        $items = array_merge($items, $defaultItems);

        return $this->render('item/index.html.twig', [
            'items' => $items,
            'user' => $this->getUser()
        ]);
    }

    /**
     * @Route("/new", name="item_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $item = new Item();
        $form = $this->createForm(ItemType::class, $item, ['postedBy' => $this->getUser()]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($item);
            $entityManager->flush();

            return $this->redirectToRoute('item_index');
        }

        return $this->render('item/new.html.twig', [
            'item' => $item,
            'form' => $form->createView(),
            'user' => $this->getUser()
        ]);
    }

    /**
     * @Route("/{id}", name="item_show", methods={"GET"})
     */
    public function show(Item $item): Response
    {
        return $this->render('item/show.html.twig', [
            'item' => $item,
            'user' => $this->getUser()
        ]);
    }

    /**
     * @Route("/{id}/edit", name="item_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Item $item, RoomItemRepository $roomItemRepository, ValueRepository $valueRepository, PropertyRepository $propertyRepository): Response
    {
        if ($item->getAuthor() == $this->getUser()) {
            $form = $this->createForm(ItemType::class, $item, array('postedBy' => $this->getUser()));
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $entityManager = $this->getDoctrine()->getManager();

                $roomItems = $roomItemRepository->findBy(array('item' => $item->getId(), 'author' => $this->getUser()->getId()));
                foreach ($roomItems as $roomItem) {
                    $vals = $valueRepository->findBy(array('roomItem' => $roomItem->getId(), 'author' => $this->getUser()->getId()));
                    foreach ($vals as $val) {
                        $entityManager->remove($val);
                    }
                    $newProperties = $item->getProperties();
                    foreach ($newProperties as $property) {
                        $value = new Value();
                        $value->setRoomItem($roomItem);
                        $value->setProperty($property);
                        $value->setValue(0);
                        $entityManager->persist($value);
                    }
                }

                $entityManager->flush();

                return $this->redirectToRoute('item_index');
            }

            return $this->render('item/edit.html.twig', [
                'item' => $item,
                'form' => $form->createView(),
                'user' => $this->getUser()
            ]);
        } else {
            return $this->redirectToRoute('item_index');
        }
    }

    /**
     * @Route("/{id}", name="item_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Item $item): Response
    {
        if ($this->isCsrfTokenValid('delete'.$item->getId(), $request->request->get('_token'))) {
            if ($item->getAuthor() == $this->getUser()) {
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->remove($item);
                $entityManager->flush();
            }
        }

        return $this->redirectToRoute('item_index');
    }

    /**
     * @Route("/list", name="item_list", methods={"GET"})
     */
    public function list(ItemRepository $itemRepository)
    {
        $items = $itemRepository->findBy(array('author' => $this->getUser()->getId()));
        $defaultItems = $itemRepository->findBy(array('author' => null));

        $items = array_merge($items, $defaultItems);

        $serializer = $this->container->get('serializer');
        $itemsJson = $serializer->serialize($items, 'json', ['groups' => ['item_list']]);

        return new Response($itemsJson);
    }
}
