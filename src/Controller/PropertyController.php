<?php

namespace App\Controller;

use App\Entity\Property;
use App\Form\PropertyType;
use App\Repository\PropertyRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/property")
 */
class PropertyController extends AbstractController
{
    /**
     * @Route("/", name="property_index", methods={"GET"})
     */
    public function index(PropertyRepository $propertyRepository): Response
    {
        $properties = $propertyRepository->findAll();
        $defaultProperties = $propertyRepository->findBy(array('author' => null));
        $properties = array_merge($properties, $defaultProperties);

        return $this->render('property/index.html.twig', [
            'properties' => $properties,
            'user' => $this->getUser()
        ]);
    }

    /**
     * @Route("/new", name="property_new", methods={"GET", "POST"})
     */
    public function new(Request $request, PropertyRepository $propertyRepository)
    {
        $property = new Property();
        $form = $this->createForm(PropertyType::class, $property);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($property->getType() == 'boolean') {
                $property->setMin(0);
                $property->setMax(1);
            }
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($property);
            $entityManager->flush();

            return $this->redirectToRoute('property_index');
        }

        return $this->render('property/new.html.twig', [
            'property' => $property,
            'form' => $form->createView(),
            'user' => $this->getUser()
        ]);
    }

    /**
     * @Route("/{id}", name="property_show", methods={"GET"})
     */
    public function show(Property $property): Response
    {
        return $this->render('property/show.html.twig', [
            'property' => $property,
            'user' => $this->getUser()
        ]);
    }

    /**
     * @Route("/{id}/edit", name="property_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Property $property, PropertyRepository $propertyRepository): Response
    {
        if ($property->getAuthor() == $this->getUser()) {
            $form = $this->createForm(PropertyType::class, $property);
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                if ($property->getType() == 'boolean') {
                    $property->setMin(0);
                    $property->setMax(1);
                }
                $this->getDoctrine()->getManager()->flush();

                return $this->redirectToRoute('property_index');
            }

            return $this->render('property/edit.html.twig', [
                'property' => $property,
                'form' => $form->createView(),
                'user' => $this->getUser()
            ]);
        } else {
            return $this->redirectToRoute('property_index');
        }
    }

    /**
     * @Route("/{id}", name="property_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Property $property): Response
    {
        if ($this->isCsrfTokenValid('delete'.$property->getId(), $request->request->get('_token'))) {
            if ($property->getAuthor() == $this->getUser()) {
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->remove($property);
                $entityManager->flush();
            }
        }

        return $this->redirectToRoute('property_index');
    }
}
