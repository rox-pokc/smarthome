<?php


namespace App\Controller;


use App\Entity\Property;
use App\Entity\RoomItem;
use App\Entity\Value;
use App\Repository\HomeRepository;
use App\Repository\ItemRepository;
use App\Repository\RoomItemRepository;
use App\Repository\RoomRepository;
use App\Repository\ValueRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * @Route("/room-item")
 */
class RoomItemController extends AbstractController
{
    /**
     * @Route("/", name="roomItem_index", methods={"GET"})
     */
    public function list(RoomItemRepository $roomItemRepository): Response
    {
        return $this->render('roomItem/index.html.twig', [
            'items' => $roomItemRepository->findAll(),
            'user' => $this->getUser()
        ]);
    }

    /**
     * @Route("/new", name="roomItem_new", methods={"GET","POST"})
     */
    public function new(Request $request, HomeRepository $homeRepository, ItemRepository $itemRepository, RoomRepository $roomRepository, SerializerInterface $serializer): Response
    {
        if ($request->isMethod("POST")) {
            $request = $request->request->all();

            if (!array_key_exists('rooms', $request)) {
                $homes = $this->listHomes($homeRepository, $serializer);
                $items = $this->listItems($itemRepository, $serializer);

                return $this->render('roomItem/new.html.twig', [
                    'homes' => $homes,
                    'items' => $items,
                    'path' => $this->generateUrl('roomItem_new'),
                    'user' => $this->getUser(),
                    'error' => new FormError('Сначала добавьте хотя бы один дом и хотя бы одну комнату в нем!')
                ]);

            }
            $this->addNew($request, $request['rooms'], $roomRepository, $itemRepository);

            return $this->redirectToRoute('home_show', ['id' => $request['homes']]);
        } else {
            $homes = $this->listHomes($homeRepository, $serializer);
            $items = $this->listItems($itemRepository, $serializer);

            return $this->render('roomItem/new.html.twig', [
                'homes' => $homes,
                'items' => $items,
                'path' => $this->generateUrl('roomItem_new'),
                'jsPath' => $this->generateUrl('homes_rooms', ['home' => 0]),
                'user' => $this->getUser(),
            ]);
        }
    }

    /**
     * @Route("/{home}/{room}/new", name="roomItem_new_with_home_room", methods={"GET", "POST"})
     */
    public function newWithHomeRoom(Request $request, $home, $room, HomeRepository $homeRepository, RoomRepository $roomRepository, ItemRepository $itemRepository, SerializerInterface $serializer)
    {
        if ($request->isMethod("POST")) {
            $request = $request->request->all();
            $this->addNew($request, $room, $roomRepository, $itemRepository);

            return $this->redirectToRoute('home_show', ['id' => $home]);

        } else {

            $items = $this->listItems($itemRepository, $serializer);

            return $this->render('roomItem/newWithHomeRoom.html.twig', [
                'home' => $homeRepository->find($home),
                'room' => $roomRepository->find($room),
                'user' => $this->getUser(),
                'items' => $items
            ]);
        }
    }

    /**
     * @Route("/{id}", name="roomItem_show", methods={"GET"})
     */
    public function show(RoomItem $item, ValueRepository $valueRepository)
    {
        $room = $item->getRoom();
        $home = $room->getHome();

        $newProperties = array();
        $properties = $item->getItem()->getProperties();
        foreach ($properties as $property) {
            $value = $valueRepository->findBy(array('roomItem' => $item->getId(), 'property' => $property->getId(), 'author' => $this->getUser()))[0];
            $value = $value->getValue();
            array_push($newProperties, array('property' => $property, 'value' => $value));
        }

        return $this->render('roomItem/show.html.twig', [
            'item' => $item,
            'properties' => $newProperties,
            'home' => $home,
            'user' => $this->getUser()
        ]);
    }

    /**
     * @Route("/{id}/edit", name="roomItem_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, RoomItem $item, RoomItemRepository $roomItemRepository, RoomRepository $roomRepository, ItemRepository $itemRepository, HomeRepository $homeRepository, ValueRepository $valueRepository)
    {
        if ($request->isMethod("POST")) {
            $item = $roomItemRepository->find($item->getId());
            $request = $request->request->all();
            $entityManager = $this->getDoctrine()->getManager();

            if ($request['items'] != $item->getItem()) {
                $properties = $item->getItem()->getProperties();
                foreach ($properties as $property) {
                    $value = $valueRepository->findBy(['property' => $property->getId(), 'roomItem' => $item->getId()])[0];
                    $entityManager->remove($value);
                }
                $newProperties = $itemRepository->find($request['items'])->getProperties();
                foreach ($newProperties as $property) {
                    $value = $this->manageValue($item, $property);
                    $entityManager->persist($value);
                }
            }

            $this->manageRoomItem($request, $request['rooms'], $roomRepository, $itemRepository, $item);

            $entityManager->flush();

            return $this->redirectToRoute('roomItem_index');

        } else {

            $homes = $this->listHomes($homeRepository);
            $items = $this->listItems($itemRepository);

            return $this->render('roomItem/edit.html.twig', [
                'homeSelected' => $item->getRoom()->getHome()->getName(),
                'homes' => $homes,
                'items' => $items,
                'itemSelected' => $item->getItem()->getName(),
                'name' =>$item->getName(),
                'path' => $this->generateUrl('roomItem_edit', ['id' => $item->getId()]),
                'user' => $this->getUser()
            ]);
        }
    }

    /**
     * @Route("/{id}", name="roomItem_delete", methods={"DELETE"})
     */
    public function delete(Request $request, RoomItem $item, ValueRepository $valueRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$item->getId(), $request->request->get('_token'))) {
            $values = $valueRepository->findBy(array('roomItem' => $item->getId(), 'author' => $this->getUser()->getId()));
            $entityManager = $this->getDoctrine()->getManager();
            foreach ($values as $value) {
                $entityManager->remove($value);
            }
            $entityManager->flush();

            $entityManager->remove($item);
            $entityManager->flush();
        }

        return $this->redirectToRoute('roomItem_index');
    }

    /**
     * @param HomeRepository $homeRepository
     * @param SerializerInterface $serializer
     * @return \App\Entity\Home[]|string
     */
    public function listHomes(HomeRepository $homeRepository)
    {
        $homes = $homeRepository->findBy(array('author' => $this->getUser()->getId()));
        return $homes;
    }

    /**
     * @param ItemRepository $itemRepository
     * @param SerializerInterface $serializer
     * @return \App\Entity\Item[]|array|string
     */
    public function listItems(ItemRepository $itemRepository)
    {
        $items = $itemRepository->findBy(array('author' => $this->getUser()->getId()));
        $defaultItems = $itemRepository->findBy(array('author' => null));
        $items = array_merge($items, $defaultItems);
        return $items;
    }

    /**
     * @param Request $request
     * @param $room
     * @param RoomRepository $roomRepository
     * @param ItemRepository $itemRepository
     * @param RoomItem $item
     */
    public function manageRoomItem($request, $room, RoomRepository $roomRepository, ItemRepository $itemRepository, RoomItem $item): void
    {
        $item->setName($request['name']);
        $item->setItem($itemRepository->find($request['items']));
        $item->setRoom($roomRepository->find($room));
    }

    /**
     * @param RoomItem $item
     * @param \App\Entity\Property $property
     * @return Value
     */
    public function manageValue(RoomItem $item, Property $property): Value
    {
        $value = new Value();
        $value->setRoomItem($item);
        $value->setProperty($property);
        $value->setValue(0);
        return $value;
    }

    /**
     * @param Request $request
     * @param $room
     * @param RoomRepository $roomRepository
     * @param ItemRepository $itemRepository
     */
    public function addNew($request, $room, RoomRepository $roomRepository, ItemRepository $itemRepository): void
    {
        $item = new RoomItem();

        $this->manageRoomItem($request, $room, $roomRepository, $itemRepository, $item);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($item);

        $properties = $item->getItem()->getProperties();
        foreach ($properties as $property) {
            $value = $this->manageValue($item, $property);
            $entityManager->persist($value);
        }
        $entityManager->flush();
    }
}