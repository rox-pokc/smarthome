<?php


namespace App\Controller;


use App\Entity\Room;
use App\Form\RoomType;
use App\Repository\RoomRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * @Route("/room")
 */
class RoomController extends AbstractController
{
    /**
     * @Route("/", name="room_index", methods={"GET"})
     */
    public function index(RoomRepository $roomRepository): Response
    {
        return $this->render('room/index.html.twig', [
            'rooms' => $roomRepository->findAll(),
            'user' => $this->getUser()
        ]);
    }

    /**
     * @Route("/new", name="room_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        return $this->redirectToRoute('room_new_with_home', [
            'home' => 0
        ]);
    }

    /**
     * @Route("/{home}/new", name="room_new_with_home", methods={"GET", "POST"})
     */
    public function newWithHome(Request $request, $home): Response
    {
        $room = new Room();
        $form = $this->createForm(RoomType::class, $room, array('postedBy' => $this->getUser(), 'home' => $home));
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($form->getData()->getHome() == null) {
                return $this->render('room/new.html.twig', [
                    'room' => $room,
                    'form' => $form->createView(),
                    'user' => $this->getUser(),
                    'error' => new FormError('Сначала добавьте хотя бы один дом!')
                ]);
            }

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($room);
            $entityManager->flush();

            return $this->redirectToRoute('room_index');
        }

        return $this->render('room/new.html.twig', [
            'room' => $room,
            'form' => $form->createView(),
            'user' => $this->getUser()
        ]);
    }

    /**
     * @Route("/{id}", name="room_show", methods={"GET"})
     */
    public function show(Room $room): Response
    {
        return $this->render('room/show.html.twig', [
            'room' => $room,
            'user' => $this->getUser()
        ]);
    }

    /**
     * @Route("/{id}/edit", name="room_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Room $room): Response
    {
        $form = $this->createForm(RoomType::class, $room, array('postedBy' => $this->getUser()));
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('room_index');
        }

        return $this->render('room/edit.html.twig', [
            'room' => $room,
            'form' => $form->createView(),
            'user' => $this->getUser()
        ]);
    }

    /**
     * @Route("/{id}", name="room_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Room $room): Response
    {
        if ($this->isCsrfTokenValid('delete'.$room->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($room);
            $entityManager->flush();
        }

        return $this->redirectToRoute('room_index');
    }

    /**
     * @Route("/homes-rooms/{home}", name="homes_rooms", methods={"GET"})
     */
    public function homesRooms($home, RoomRepository $roomRepository, SerializerInterface $serializer)
    {
        $rooms = $roomRepository->findBy(array('author' => $this->getUser()->getId(), 'home' => $home));

        $roomsJson = $serializer->serialize($rooms, 'json', ['groups' => ['homes_rooms']]);

        return new Response($roomsJson);
    }
}
