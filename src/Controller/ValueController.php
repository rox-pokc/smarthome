<?php


namespace App\Controller;


use App\Repository\PropertyRepository;
use App\Repository\RoomItemRepository;
use App\Repository\ValueRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/value")
 */
class ValueController extends AbstractController
{
    /**
     * @Route("/update", name="value_update", methods={"POST"})
     */
    public function update(Request $request, ValueRepository $valueRepository, RoomItemRepository $roomItemRepository, PropertyRepository $propertyRepository)
    {
        $request = $request->request->all();
        $roomItem = $roomItemRepository->find($request['roomItem']);
        foreach ($request['properties'] as $key => $val) {
            $property = $propertyRepository->find($key);
            $value = $valueRepository->findOneBy(array('roomItem' => $roomItem->getId(), 'property' => $property->getId(), 'author' => $this->getUser()->getId()));
            if ($property->getType() == 'boolean' && $val == 'On') {
                $value->setValue(1);
            }
            else if ($property->getType() == 'boolean' && $val == 'Off') {
                $value->setValue(0);
            }
            else {
                $value->setValue($val);
            }

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->flush();
        }

        return $this->redirectToRoute('roomItem_show', ['id' => $roomItem->getId()]);
    }
}