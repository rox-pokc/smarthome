<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190826102800 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE room (id INT AUTO_INCREMENT NOT NULL, home_id INT NOT NULL, name VARCHAR(255) NOT NULL, INDEX IDX_729F519B28CDC89C (home_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, date_of_birth INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE home (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, name VARCHAR(255) NOT NULL, INDEX IDX_71D60CD0A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE value (id INT AUTO_INCREMENT NOT NULL, room_item_id INT NOT NULL, property_id INT NOT NULL, value VARCHAR(255) NOT NULL, INDEX IDX_1D775834FE4DEEBE (room_item_id), INDEX IDX_1D775834549213EC (property_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE property (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, type VARCHAR(255) NOT NULL, min INT DEFAULT NULL, max INT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE property_item (property_id INT NOT NULL, item_id INT NOT NULL, INDEX IDX_39A24549213EC (property_id), INDEX IDX_39A24126F525E (item_id), PRIMARY KEY(property_id, item_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE room_item (id INT AUTO_INCREMENT NOT NULL, room_id INT NOT NULL, item_id INT NOT NULL, name VARCHAR(255) NOT NULL, INDEX IDX_7C1FCF7A54177093 (room_id), INDEX IDX_7C1FCF7A126F525E (item_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE item (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE room ADD CONSTRAINT FK_729F519B28CDC89C FOREIGN KEY (home_id) REFERENCES home (id)');
        $this->addSql('ALTER TABLE home ADD CONSTRAINT FK_71D60CD0A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE value ADD CONSTRAINT FK_1D775834FE4DEEBE FOREIGN KEY (room_item_id) REFERENCES room_item (id)');
        $this->addSql('ALTER TABLE value ADD CONSTRAINT FK_1D775834549213EC FOREIGN KEY (property_id) REFERENCES property (id)');
        $this->addSql('ALTER TABLE property_item ADD CONSTRAINT FK_39A24549213EC FOREIGN KEY (property_id) REFERENCES property (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE property_item ADD CONSTRAINT FK_39A24126F525E FOREIGN KEY (item_id) REFERENCES item (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE room_item ADD CONSTRAINT FK_7C1FCF7A54177093 FOREIGN KEY (room_id) REFERENCES room (id)');
        $this->addSql('ALTER TABLE room_item ADD CONSTRAINT FK_7C1FCF7A126F525E FOREIGN KEY (item_id) REFERENCES item (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE room_item DROP FOREIGN KEY FK_7C1FCF7A54177093');
        $this->addSql('ALTER TABLE home DROP FOREIGN KEY FK_71D60CD0A76ED395');
        $this->addSql('ALTER TABLE room DROP FOREIGN KEY FK_729F519B28CDC89C');
        $this->addSql('ALTER TABLE value DROP FOREIGN KEY FK_1D775834549213EC');
        $this->addSql('ALTER TABLE property_item DROP FOREIGN KEY FK_39A24549213EC');
        $this->addSql('ALTER TABLE value DROP FOREIGN KEY FK_1D775834FE4DEEBE');
        $this->addSql('ALTER TABLE property_item DROP FOREIGN KEY FK_39A24126F525E');
        $this->addSql('ALTER TABLE room_item DROP FOREIGN KEY FK_7C1FCF7A126F525E');
        $this->addSql('DROP TABLE room');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE home');
        $this->addSql('DROP TABLE value');
        $this->addSql('DROP TABLE property');
        $this->addSql('DROP TABLE property_item');
        $this->addSql('DROP TABLE room_item');
        $this->addSql('DROP TABLE item');
    }
}
