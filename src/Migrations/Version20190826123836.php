<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190826123836 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('INSERT INTO `property` (`id`, `name`, `type`, `min`, `max`) VALUES
                    (1, \'Состояние\', \'boolean\', 0, 1),
                    (2, \'Яркость\', \'integer\', 0, 100),
                    (3, \'Температура\', \'integer\', -30, 30),
                    (4, \'Интенсивность\', \'integer\', 0, 100),
                    (5, \'Громкость\', \'integer\', 0, 100);'
        );

        $this->addSql('INSERT INTO `item` (`id`, `name`) VALUES
                    (1, \'Сигнализация\'),
                    (2, \'Розетка\'),
                    (3, \'Лампочка\'),
                    (4, \'Робот-пылесос\'),
                    (5, \'Кондиционер\'),
                    (6, \'Роутер\'),
                    (7, \'Ночник\'),
                    (8, \'Очиститель воздуха\'),
                    (9, \'Веб-камера\'),
                    (10, \'Дверной замок\'),
                    (11, \'Колонка\'),
                    (12, \'Холодильник\'),
                    (13, \'Обогреватель\');'
        );

        $this->addSql('INSERT INTO `property_item` (`property_id`, `item_id`) VALUES
                    (1, 1),
                    (1, 2),
                    (1, 3),
                    (1, 4),
                    (1, 5),
                    (1, 6),
                    (1, 7),
                    (1, 8),
                    (1, 9),
                    (1, 10),
                    (1, 11),
                    (1, 12),
                    (1, 13),
                    (2, 3),
                    (2, 7),
                    (3, 5),
                    (3, 12),
                    (3, 13),
                    (4, 5),
                    (4, 8),
                    (5, 11);'
        );
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
    }
}
