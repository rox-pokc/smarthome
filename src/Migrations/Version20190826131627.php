<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190826131627 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE home ADD author_id INT DEFAULT NULL, ADD created_at INT DEFAULT 0 NOT NULL');
        $this->addSql('ALTER TABLE home ADD CONSTRAINT FK_71D60CD0F675F31B FOREIGN KEY (author_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_71D60CD0F675F31B ON home (author_id)');
        $this->addSql('ALTER TABLE item ADD author_id INT DEFAULT NULL, ADD created_at INT DEFAULT 0 NOT NULL');
        $this->addSql('ALTER TABLE item ADD CONSTRAINT FK_1F1B251EF675F31B FOREIGN KEY (author_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_1F1B251EF675F31B ON item (author_id)');
        $this->addSql('ALTER TABLE room ADD author_id INT DEFAULT NULL, ADD created_at INT DEFAULT 0 NOT NULL');
        $this->addSql('ALTER TABLE room ADD CONSTRAINT FK_729F519BF675F31B FOREIGN KEY (author_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_729F519BF675F31B ON room (author_id)');
        $this->addSql('ALTER TABLE value ADD author_id INT DEFAULT NULL, ADD created_at INT DEFAULT 0 NOT NULL');
        $this->addSql('ALTER TABLE value ADD CONSTRAINT FK_1D775834F675F31B FOREIGN KEY (author_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_1D775834F675F31B ON value (author_id)');
        $this->addSql('ALTER TABLE property ADD author_id INT DEFAULT NULL, ADD created_at INT DEFAULT 0 NOT NULL');
        $this->addSql('ALTER TABLE property ADD CONSTRAINT FK_8BF21CDEF675F31B FOREIGN KEY (author_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_8BF21CDEF675F31B ON property (author_id)');
        $this->addSql('ALTER TABLE room_item ADD author_id INT DEFAULT NULL, ADD created_at INT DEFAULT 0 NOT NULL');
        $this->addSql('ALTER TABLE room_item ADD CONSTRAINT FK_7C1FCF7AF675F31B FOREIGN KEY (author_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_7C1FCF7AF675F31B ON room_item (author_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE home DROP FOREIGN KEY FK_71D60CD0F675F31B');
        $this->addSql('DROP INDEX IDX_71D60CD0F675F31B ON home');
        $this->addSql('ALTER TABLE home DROP author_id, DROP created_at');
        $this->addSql('ALTER TABLE item DROP FOREIGN KEY FK_1F1B251EF675F31B');
        $this->addSql('DROP INDEX IDX_1F1B251EF675F31B ON item');
        $this->addSql('ALTER TABLE item DROP author_id, DROP created_at');
        $this->addSql('ALTER TABLE property DROP FOREIGN KEY FK_8BF21CDEF675F31B');
        $this->addSql('DROP INDEX IDX_8BF21CDEF675F31B ON property');
        $this->addSql('ALTER TABLE property DROP author_id, DROP created_at');
        $this->addSql('ALTER TABLE room DROP FOREIGN KEY FK_729F519BF675F31B');
        $this->addSql('DROP INDEX IDX_729F519BF675F31B ON room');
        $this->addSql('ALTER TABLE room DROP author_id, DROP created_at');
        $this->addSql('ALTER TABLE room_item DROP FOREIGN KEY FK_7C1FCF7AF675F31B');
        $this->addSql('DROP INDEX IDX_7C1FCF7AF675F31B ON room_item');
        $this->addSql('ALTER TABLE room_item DROP author_id, DROP created_at');
        $this->addSql('ALTER TABLE value DROP FOREIGN KEY FK_1D775834F675F31B');
        $this->addSql('DROP INDEX IDX_1D775834F675F31B ON value');
        $this->addSql('ALTER TABLE value DROP author_id, DROP created_at');
    }
}
