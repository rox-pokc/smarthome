<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190901113254 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('INSERT INTO `user`(`id`, `name`, `date_of_birth`, `phone`, `roles`, `password`, `created_at`) VALUES (1, \'test\', 946684800, \'test\', JSON_ARRAY(), \'$argon2id$v=19$m=65536,t=4,p=1$55YPU/ZBVEgUz0Tl7W6O3g$M9pmUF2pALcjKT+JauQ35AWFvsSavgB7rBWugL4UwdI\',1567335010)');
    }

    public function down(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DELETE FROM `user` WHERE `id`=1');
    }
}
