<?php


namespace App\Entity;


use App\Helper\AuthorInterface;
use App\Helper\AuthorTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ItemRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Item implements AuthorInterface
{
    use AuthorTrait;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups("item_list")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups("item_list")
     */
    private $name;

    /**
     * @var Property[]|ArrayCollection
     * @ORM\ManyToMany(targetEntity="App\Entity\Property", mappedBy="items")
     */
    private $properties;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\RoomItem", mappedBy="item")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $roomItems;

    public function __construct()
    {
        $this->properties = new ArrayCollection();
        $this->roomItems = new ArrayCollection();
        $this->setCreatedAt(0);
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Property[]
     */
    public function getProperties(): Collection
    {
        return $this->properties;
    }

    public function addProperty(Property $property): self
    {
        if (!$this->properties->contains($property)) {
            $this->properties[] = $property;
            $property->addItem($this);
        }

        return $this;
    }

    public function removeProperty(Property $property): self
    {
        if ($this->properties->contains($property)) {
            $this->properties->removeElement($property);
            $property->removeItem($this);
        }

        return $this;
    }

    /**
     * @return Collection|RoomItem[]
     */
    public function getRoomItems(): Collection
    {
        return $this->roomItems;
    }

    public function addRoomItem(RoomItem $roomItem): self
    {
        if (!$this->roomItems->contains($roomItem)) {
            $this->roomItems[] = $roomItem;
            $roomItem->setItem($this);
        }

        return $this;
    }

    public function removeRoomItem(RoomItem $roomItem): self
    {
        if ($this->roomItems->contains($roomItem)) {
            $this->roomItems->removeElement($roomItem);
            // set the owning side to null (unless already changed)
            if ($roomItem->getItem() === $this) {
                $roomItem->setItem(null);
            }
        }

        return $this;
    }
}
