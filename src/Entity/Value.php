<?php


namespace App\Entity;


use App\Helper\AuthorInterface;
use App\Helper\AuthorTrait;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ValueRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Value implements AuthorInterface
{
    use AuthorTrait;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups("value_by_item_property")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\RoomItem", inversedBy="vals")
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     * @Groups("value_by_item_property")
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    private $roomItem;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Property", inversedBy="vals")
     * @ORM\JoinColumn(nullable=false)
     * @Groups("value_by_item_property")
     */
    private $property;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups("value_by_item_property")
     */
    private $value;

    public function __construct()
    {
        $this->setCreatedAt(0);
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRoomItem(): ?RoomItem
    {
        return $this->roomItem;
    }

    public function setRoomItem(?RoomItem $roomItem): self
    {
        $this->roomItem = $roomItem;

        return $this;
    }

    public function getProperty(): ?Property
    {
        return $this->property;
    }

    public function setProperty(?Property $property): self
    {
        $this->property = $property;

        return $this;
    }

    public function getValue(): ?string
    {
        return $this->value;
    }

    public function setValue(string $value): self
    {
        $this->value = $value;

        return $this;
    }
}
