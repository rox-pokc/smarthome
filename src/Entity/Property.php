<?php


namespace App\Entity;


use App\Helper\AuthorInterface;
use App\Helper\AuthorTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PropertyRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Property implements AuthorInterface
{
    use AuthorTrait;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups("value_by_item_property")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $type;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $min;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $max;

    /**
     * @var Item[]|ArrayCollection
     * @ORM\ManyToMany(targetEntity="App\Entity\Item", inversedBy="properties")
     */
    private $items;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Value", mappedBy="property")
     */
    private $vals;

    public function __construct()
    {
        $this->items = new ArrayCollection();
        $this->vals = new ArrayCollection();
        $this->setCreatedAt(0);
    }

    public function __toString()
    {
        // TODO: Implement __toString() method.
        return (string)$this->getName();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getMin(): ?int
    {
        return $this->min;
    }

    public function setMin(?int $min): self
    {
        $this->min = $min;

        return $this;
    }

    public function getMax(): ?int
    {
        return $this->max;
    }

    public function setMax(?int $max): self
    {
        $this->max = $max;

        return $this;
    }

    /**
     * @return Collection|Item[]
     */
    public function getItems(): Collection
    {
        return $this->items;
    }

    public function addItem(Item $item): self
    {
        if (!$this->items->contains($item)) {
            $this->items[] = $item;
        }

        return $this;
    }

    public function removeItem(Item $item): self
    {
        if ($this->items->contains($item)) {
            $this->items->removeElement($item);
        }

        return $this;
    }

    /**
     * @return Collection|Value[]
     */
    public function getVals(): Collection
    {
        return $this->vals;
    }

    public function addVal(Value $val): self
    {
        if (!$this->vals->contains($val)) {
            $this->vals[] = $val;
            $val->setProperty($this);
        }

        return $this;
    }

    public function removeVal(Value $val): self
    {
        if ($this->vals->contains($val)) {
            $this->vals->removeElement($val);
            // set the owning side to null (unless already changed)
            if ($val->getProperty() === $this) {
                $val->setProperty(null);
            }
        }

        return $this;
    }
}
