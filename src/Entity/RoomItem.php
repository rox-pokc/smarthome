<?php


namespace App\Entity;


use App\Helper\AuthorInterface;
use App\Helper\AuthorTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RoomItemRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class RoomItem implements AuthorInterface
{
    use AuthorTrait;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups("value_by_item_property")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Room", inversedBy="roomItems")
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    private $room;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Item", inversedBy="roomItems")
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    private $item;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Value", mappedBy="roomItem")
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    private $vals;

    public function __construct()
    {
        $this->vals = new ArrayCollection();
        $this->setCreatedAt(0);
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRoom(): ?Room
    {
        return $this->room;
    }

    public function setRoom(?Room $room): self
    {
        $this->room = $room;

        return $this;
    }

    public function getItem(): ?Item
    {
        return $this->item;
    }

    public function setItem(?Item $item): self
    {
        $this->item = $item;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Value[]
     */
    public function getVals(): Collection
    {
        return $this->vals;
    }

    public function addVal(Value $val): self
    {
        if (!$this->vals->contains($val)) {
            $this->vals[] = $val;
            $val->setRoomItem($this);
        }

        return $this;
    }

    public function removeVal(Value $val): self
    {
        if ($this->vals->contains($val)) {
            $this->vals->removeElement($val);
            // set the owning side to null (unless already changed)
            if ($val->getRoomItem() === $this) {
                $val->setRoomItem(null);
            }
        }

        return $this;
    }
}