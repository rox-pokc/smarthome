<?php

namespace App\Entity;

use App\Helper\AuthorInterface;
use App\Helper\AuthorTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RoomRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Room implements AuthorInterface
{
    use AuthorTrait;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups("homes_rooms")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups("homes_rooms")
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Home", inversedBy="rooms")
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    private $home;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\RoomItem", mappedBy="room")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $roomItems;

    public function __construct()
    {
        $this->roomItems = new ArrayCollection();
        $this->setCreatedAt(0);
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getHome(): ?Home
    {
        return $this->home;
    }

    public function setHome(?Home $home): self
    {
        $this->home = $home;

        return $this;
    }

    /**
     * @return Collection|RoomItem[]
     */
    public function getRoomItems(): Collection
    {
        return $this->roomItems;
    }

    public function addRoomItem(RoomItem $roomItem): self
    {
        if (!$this->roomItems->contains($roomItem)) {
            $this->roomItems[] = $roomItem;
            $roomItem->setRoom($this);
        }

        return $this;
    }

    public function removeRoomItem(RoomItem $roomItem): self
    {
        if ($this->roomItems->contains($roomItem)) {
            $this->roomItems->removeElement($roomItem);
            // set the owning side to null (unless already changed)
            if ($roomItem->getRoom() === $this) {
                $roomItem->setRoom(null);
            }
        }

        return $this;
    }
}