const minDiv = document.getElementsByName('property[min]')[0].parentElement;
const maxDiv = document.getElementsByName('property[max]')[0].parentElement;

const hideMinMaxValues = function() {
    let list = document.getElementsByName('property[type]')[0];
    let option = list.options[list.selectedIndex].value;
    if (option == 'boolean') {
        minDiv.hidden = true;
        maxDiv.hidden = true;
    } else {
        minDiv.hidden = false;
        maxDiv.hidden = false;
    }
};

hideMinMaxValues();
document.getElementsByName('property[type]')[0].addEventListener('change', hideMinMaxValues);

