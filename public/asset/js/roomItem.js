const updateRoomsList = function () {
    const homes = document.getElementsByName('homes')[0];
    const home = homes.options[homes.selectedIndex].value;
    const rooms = document.getElementsByName('rooms')[0];
    const jsPath = document.getElementsByName('jsPath')[0].dataset.content;

    fetch(jsPath.substr(0, jsPath.length - 1) + home)
        .then(res => res.json())
            .then((data) => {
                rooms.innerHTML = '';
                for (key in data) {
                    let option = document.createElement("option");
                    option.value = data[key].id;
                    option.textContent = data[key].name;
                    rooms.appendChild(option);
                }
            });
};

document.getElementsByName('homes')[0].addEventListener("change", updateRoomsList);
updateRoomsList();
