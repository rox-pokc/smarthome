<?php


namespace App\Tests;


use App\Entity\Property;

class PropertyControllerTest extends Login
{
    public function testIndex()
    {
        $client = $this->login();
        $client->request('GET', '/property/');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    /**
     * @dataProvider provideSampleProperties
     */
    public function testNew($samplePropertyName, $samplePropertyType)
    {
        $client = $this->login();

        //new
        $propertyRepository = static::$container->get('doctrine.orm.entity_manager')->getRepository(Property::class);
        $client->request(
            'POST',
            '/property/new',
            array(
                "name" => $samplePropertyName,
                "type" => $samplePropertyType,
            )
        );
        $this->assertEquals(302, $client->getResponse()->getStatusCode());
        $property = $propertyRepository->findBy(array(),array('id'=>'DESC'), 1, 0)[0];
        $this->assertEquals($samplePropertyName, $property->getName());
        $this->assertEquals($samplePropertyType, $property->getType());

        //edit
        $samplePropertyName[0] = 'q';
        $client->request(
            'POST',
            '/property/'.$property->getId().'/edit',
            array(
                "name" => $samplePropertyName,
                "type" => $samplePropertyType,
            )
        );
        $this->assertEquals(302, $client->getResponse()->getStatusCode());
        $property = $propertyRepository->findBy(array(),array('id'=>'DESC'), 1, 0)[0];
        $this->assertEquals($samplePropertyName, $property->getName());
        $this->assertEquals($samplePropertyType, $property->getType());

        $qb = $propertyRepository->createQueryBuilder('e');
        $qb->delete()->where($qb->expr()->eq('e.id',':id'));
        $qb->setParameter(':id', $property->getId());
        $qb->getQuery()->getResult();
    }

    public function provideSampleProperties()
    {
        return [
            [
                'name' => 'a',
                'type' => 'a'
            ],
            [
                'name' => 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
                'type' => 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa'
            ]
        ];
    }
}