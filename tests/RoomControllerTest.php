<?php


namespace App\Tests;


use App\Entity\Home;
use App\Entity\Room;

class RoomControllerTest extends Login
{
    public function testIndex()
    {
        $client = $this->login();
        $client->request('GET', '/room/');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    /**
     * @dataProvider provideSampleRooms
     */
    public function testNew($sampleRoomName)
    {
        $client = $this->login();

        $client->request(
            'POST',
            '/home/new'
        );
        $client->submitForm('Сохранить', [
            "home[name]" => 'test'
        ]);
        $home = static::$container->get('doctrine.orm.entity_manager')->getRepository(Home::class)->findBy(array('name' => 'test'))[0];

        //new
        $roomRepository = static::$container->get('doctrine.orm.entity_manager')->getRepository(Room::class);
        $client->request(
            'POST',
            '/room/new'
        );
        $client->submitForm('Сохранить', [
            "room[home]" => $home->getId(),
            "room[name]" => $sampleRoomName
        ]);
        $this->assertEquals(302, $client->getResponse()->getStatusCode());
        $room = $roomRepository->findBy(array(),array('id'=>'DESC'), 1, 0)[0];
        $this->assertEquals($sampleRoomName, $room->getName());
        $this->assertEquals($home->getId(), $room->getHome()->getId());

        //edit
        $sampleRoomName[0] = 'q';
        $client->request(
            'POST',
            '/room/'.$room->getId().'/edit'
        );
        $client->submitForm('Изменить', [
            "room[home]" => $home->getId(),
            "room[name]" => $sampleRoomName,
        ]);
        $roomRepository = static::$container->get('doctrine.orm.entity_manager')->getRepository(Room::class);
        $this->assertEquals(302, $client->getResponse()->getStatusCode());
        $room = $roomRepository->findBy(array(),array('id'=>'DESC'), 1, 0)[0];
        $this->assertEquals($sampleRoomName, $room->getName());
        $this->assertEquals($home->getId(), $room->getHome()->getId());

        //delete
        $amount = $roomRepository->counting();
        $token = static::$container->get('security.csrf.token_manager')->getToken('delete'.$room->getId());
        $client->request(
            'DELETE',
            '/room/'.$room->getId(),
            array(
                '_method' => 'DELETE',
                '_token' => $token
            ),
            array(),
            array('room' => $home)
        );
        $roomRepository = static::$container->get('doctrine.orm.entity_manager')->getRepository(Room::class);
        $newAmount = $roomRepository->counting();
        $this->assertEquals(302, $client->getResponse()->getStatusCode());
        $this->assertEquals(1, $amount-$newAmount);

        $homeRepository = static::$container->get('doctrine.orm.entity_manager')->getRepository(Home::class);
        $qb = $homeRepository->createQueryBuilder('e');
        $qb->delete()->where($qb->expr()->eq('e.id',':id'));
        $qb->setParameter(':id', $home->getId());
        $qb->getQuery()->getResult();
    }

    public function provideSampleRooms()
    {
        return [
            ['a'],
            ['aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa']
        ];
    }
}