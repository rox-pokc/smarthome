<?php


namespace App\Tests;


use App\Entity\Home;
use App\Entity\User;

class HomeControllerTest extends Login
{
    public function testIndex()
    {
        $client = $this->login();
        $client->request('GET', '/home/');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    /**
     * @dataProvider provideSampleHomes
     */
    public function testNew($sampleHomeName)
    {
        $client = $this->login();

        //new
        $homeRepository = static::$container->get('doctrine.orm.entity_manager')->getRepository(Home::class);
        $client->request(
            'POST',
            '/home/new'
        );
        $client->submitForm('Сохранить', [
            "home[name]" => $sampleHomeName,
        ]);
        $this->assertEquals(302, $client->getResponse()->getStatusCode());
        $home = $homeRepository->findBy(array(),array('id'=>'DESC'), 1, 0)[0];
        $this->assertEquals($sampleHomeName, $home->getName());
        $this->assertEquals(1, $home->getUser()->getId());

        //edit
        $sampleHomeName[0] = 'q';
        $client->request(
            'POST',
            '/home/'.$home->getId().'/edit'
        );
        $client->submitForm('Изменить', [
            "home[name]" => $sampleHomeName,
        ]);
        $homeRepository = static::$container->get('doctrine.orm.entity_manager')->getRepository(Home::class);
        $this->assertEquals(302, $client->getResponse()->getStatusCode());
        $home = $homeRepository->findBy(array(),array('id'=>'DESC'), 1, 0)[0];
        $this->assertEquals($sampleHomeName, $home->getName());
        $this->assertEquals(1, $home->getUser()->getId());

        //delete
        $amount = $homeRepository->counting();
        $token = static::$container->get('security.csrf.token_manager')->getToken('delete'.$home->getId());
        $client->request(
            'DELETE',
            '/home/'.$home->getId(),
            array(
                '_method' => 'DELETE',
                '_token' => $token
            ),
            array(),
            array('home' => $home)
        );
        $homeRepository = static::$container->get('doctrine.orm.entity_manager')->getRepository(Home::class);
        $newAmount = $homeRepository->counting();
        $this->assertEquals(302, $client->getResponse()->getStatusCode());
        $this->assertEquals(1, $amount-$newAmount);
    }

    public function provideSampleHomes()
    {
        return [
            ['a'],
            ['aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa']
        ];
    }
}