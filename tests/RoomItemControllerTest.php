<?php


namespace App\Tests;


use App\Entity\Home;
use App\Entity\Item;
use App\Entity\Room;
use App\Entity\RoomItem;

class RoomItemControllerTest extends Login
{

    public function testIndex()
    {
        $client = $this->login();
        $client->request('GET', '/room-item/');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    /**
     * @dataProvider provideSampleRoomsItems
     */
    public function testNew($sampleRooomItemName)
    {
        $client = $this->login();

        $client->request(
            'POST',
            '/home/new'
        );
        $client->submitForm('Сохранить', [
            "home[name]" => 'test'
        ]);
        $home = static::$container->get('doctrine.orm.entity_manager')->getRepository(Home::class)->findBy(array('name' => 'test'))[0];
        $client->request(
            'POST',
            '/room/new'
        );
        $client->submitForm('Сохранить', [
            "room[home]" => $home->getId(),
            "room[name]" => 'test'
        ]);
        $room = static::$container->get('doctrine.orm.entity_manager')->getRepository(Room::class)->findBy(array('name' => 'test'))[0];
        $client->request(
            'POST',
            '/item/new'
        );
        $client->submitForm('Сохранить', [
            "item[name]" => 'test',
        ]);
        $item = static::$container->get('doctrine.orm.entity_manager')->getRepository(Item::class)->findBy(array('name' => 'test'))[0];


        //new
        $roomItemRepository = static::$container->get('doctrine.orm.entity_manager')->getRepository(RoomItem::class);
        $client->request(
            'POST',
            '/room-item/new',
            array(
                'homes' => $home->getId(),
                'rooms' => $room->getId(),
                'items' => $item->getId(),
                'name' => $sampleRooomItemName
            )
        );
        $this->assertEquals(302, $client->getResponse()->getStatusCode());
        $roomItem = $roomItemRepository->findBy(array(),array('id'=>'DESC'), 1, 0)[0];
        $this->assertEquals($sampleRooomItemName, $roomItem->getName());
        $this->assertEquals($room->getId(), $roomItem->getRoom()->getId());
        $this->assertEquals($item->getId(), $roomItem->getItem()->getId());

        //edit
        $sampleRooomItemName[0] = 'q';
        $client->request(
            'POST',
            '/room-item/'.$roomItem->getId().'/edit',
            array(
                'homes' => $home->getId(),
                'rooms' => $room->getId(),
                'items' => $item->getId(),
                'name' => $sampleRooomItemName
            )
        );
        $roomItemRepository = static::$container->get('doctrine.orm.entity_manager')->getRepository(RoomItem::class);
        $this->assertEquals(302, $client->getResponse()->getStatusCode());
        $roomItem = $roomItemRepository->findBy(array(),array('id'=>'DESC'), 1, 0)[0];
        $this->assertEquals($sampleRooomItemName, $roomItem->getName());
        $this->assertEquals($room->getId(), $roomItem->getRoom()->getId());
        $this->assertEquals($item->getId(), $roomItem->getItem()->getId());

        $qb = $roomItemRepository->createQueryBuilder('e');
        $qb->delete()->where($qb->expr()->eq('e.id',':id'));
        $qb->setParameter(':id', $roomItem->getId());
        $qb->getQuery()->getResult();

        $qb = static::$container->get('doctrine.orm.entity_manager')->getRepository(Item::class)->createQueryBuilder('e');
        $qb->delete()->where($qb->expr()->eq('e.id',':id'));
        $qb->setParameter(':id', $item->getId());
        $qb->getQuery()->getResult();

        $qb = static::$container->get('doctrine.orm.entity_manager')->getRepository(Room::class)->createQueryBuilder('e');
        $qb->delete()->where($qb->expr()->eq('e.id',':id'));
        $qb->setParameter(':id', $room->getId());
        $qb->getQuery()->getResult();

        $qb = static::$container->get('doctrine.orm.entity_manager')->getRepository(Home::class)->createQueryBuilder('e');
        $qb->delete()->where($qb->expr()->eq('e.id',':id'));
        $qb->setParameter(':id', $home->getId());
        $qb->getQuery()->getResult();
    }

    public function provideSampleRoomsItems()
    {
        return [
            ['a'],
            ['aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa']
        ];
    }
}