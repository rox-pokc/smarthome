<?php


namespace App\Tests;


use App\Entity\Item;

class ItemControllerTest extends Login
{
    public function testIndex()
    {
        $client = $this->login();
        $client->request('GET', '/item/');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    /**
     * @dataProvider provideSampleItems
     */
    public function testNew($sampleItemName)
    {
        $client = $this->login();

        //new
        $itemRepository = static::$container->get('doctrine.orm.entity_manager')->getRepository(Item::class);
        $client->request(
            'POST',
            '/item/new'
        );
        $client->submitForm('Сохранить', [
            "item[name]" => $sampleItemName,
        ]);
        $this->assertEquals(302, $client->getResponse()->getStatusCode());
        $item = $itemRepository->findBy(array(),array('id'=>'DESC'), 1, 0)[0];
        $this->assertEquals($sampleItemName, $item->getName());

        //edit
        $sampleItemName[0] = 'q';
        $client->request(
            'POST',
            '/item/'.$item->getId().'/edit'
        );
        $client->submitForm('Изменить', [
            "item[name]" => $sampleItemName,
        ]);
        $itemRepository = static::$container->get('doctrine.orm.entity_manager')->getRepository(Item::class);
        $this->assertEquals(302, $client->getResponse()->getStatusCode());
        $item = $itemRepository->findBy(array(),array('id'=>'DESC'), 1, 0)[0];
        $this->assertEquals($sampleItemName, $item->getName());

        //delete
        $amount = $itemRepository->counting();
        $token = static::$container->get('security.csrf.token_manager')->getToken('delete'.$item->getId());
        $client->request(
            'DELETE',
            '/item/'.$item->getId(),
            array(
                '_method' => 'DELETE',
                '_token' => $token
            ),
            array(),
            array('item' => $item)
        );
        $itemRepository = static::$container->get('doctrine.orm.entity_manager')->getRepository(Item::class);
        $newAmount = $itemRepository->counting();
        $this->assertEquals(302, $client->getResponse()->getStatusCode());
        $this->assertEquals(1, $amount-$newAmount);
    }

    public function provideSampleItems()
    {
        return [
            ['a'],
            ['aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa']
        ];
    }
}