<?php


namespace App\Tests;


class MainControllerTest extends Login
{
    public function testIndex()
    {
        $client = $this->login();
        $client->request('GET', '/');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }
}